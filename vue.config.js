// const IS_PRODUCTION = process.env.NODE_ENV === 'production'
 // vue.config.js

const VuetifyLoaderPlugin = require('vuetify-loader/lib/plugin')
module.exports = {
    pwa: {
      name: ' XP',
      themeColor: '#000000',
      msTileColor: '#000000',
      appleMobileWebAppCapable: 'yes',
      appleMobileWebAppStatusBarStyle: 'black',

      iconPaths: {
        favicon32: 'static/favicon-32x32.png',
        favicon16: 'static/favicon-16x16.png',
        appleTouchIcon: 'static/apple-icon-152x152.png',
        maskIcon: 'static/apple-icon-precomposed.svg',
        msTileImage: 'static/ms-icon-144x144.png',
      },

      // configure the workbox plugin
      workboxPluginMode: 'InjectManifest',
      workboxOptions: {
        // swSrc is required in InjectManifest mode.
        swSrc: 'service-worker.js',
        // ...other Workbox options...
      }
    },
    
    configureWebpack: {
      plugins: [
        new VuetifyLoaderPlugin()
      ], 
    },
    outputDir: 'dir',
    assetsDir: 'static',
    // baseUrl: IS_PRODUCTION
    // ? 'http://cdn123.com'
    // : '/',
    // For Production, replace set baseUrl to CDN
    // And set the CDN origin to `yourdomain.com/static`
    // Whitenoise will serve once to CDN which will then cache
    // and distribute
    devServer: {
      proxy: {
        '/api*': {
          // Forward frontend dev server request for /api to django dev server
          target: 'http://localhost:8000/',
        },
        '/*': {
          // Forward frontend dev server request for /api to django dev server
          target: 'http://localhost:8000/',
        }
      }
    }
  } 
