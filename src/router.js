import Vue from 'vue'
import Router from 'vue-router'
import Messages from '@/components/Messages'
import Home from '@/pages/home'
import Authorize from '@/pages/authorize'
import CreateEvents from '@/pages/events/create'
import ViewEvents from '@/pages/events/view'
import Search from '@/pages/search'
import Feed from '@/pages/feed'
import checkoutSuccess from '@/pages/checkout/success'
import Cancel from '@/pages/checkout/cancel'
import ViewProfile from '@/pages/profile/view'
import connectStripe from '@/pages/authorize/components/Stripe'
import Dashboard from '@/pages/dashboard'
import Verify from '@/pages/verify'
import NotFound from '@/pages/404'
import Settings from '@/pages/settings'
import TipSuccess from '@/pages/feed/components/TipSuccess'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Feed
    },
    {
      path: '/p/:uuid',
      name: 'post',
      component: Feed
    },
    {
      path: '/x/reversegeocode',
      name: 'reversegeocode',
      component: Home
    },
    {
      path: '/messages',
      name: 'messages',
      component: Messages
    },
    {
      path: '/authorize',
      name: 'authorize',
      component: Authorize
    },
    {
      path: '/c/events',
      name: 'createEvents',
      component: CreateEvents
    },
    {
      path: '/e/event/:uuid',
      name: 'editEvent',
      component: CreateEvents
    },
    {
      path: '/event/:uuid',
      name: 'viewEvents',
      component: ViewEvents
    },
    {
      path: '/event/:uuid/:pid',
      name: 'pViewEvents',
      component: ViewEvents
    },
    {
      path: '/stripe/oauth',
      name: 'connectStripe',
      component: connectStripe
    },
    {
      path: '/success/:session',
      name: 'checkoutSuccess',
      component: checkoutSuccess
    },
    {
      path: '/profile',
      name: 'profile',
      component: ViewProfile
    },
    {
      path: '/e/cancel',
      name: 'cancel',
      component: Cancel
    },
    {
      path: '/u/:username',
      name: 'viewUser',
      component: ViewProfile
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/verify',
      name: 'verify',
      component: Verify
    },
    {
      path: '/search',
      name: 'search',
      component: Search
    },
    {
      path: '/tip/success/:session',
      name: 'tipSuccess',
      component: TipSuccess
    },
    {
      path: '/404',
      name: '404',
      component: NotFound
    },
    {
      path: '/settings',
      name: 'settings',
      component: Settings
    }
  ]
}) 
