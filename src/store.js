// ./src/store.js

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import Cookies from 'js-cookie' 
import router from '@/router'

Vue.use(Vuex, axios)

export default new Vuex.Store ({
  state: {
    authenticated: false,
    user: null,
    events: null,
    total: null,
    bag: [],
    session: {},
    XP: null,
    users: [],
    profiles: null,
    profile: null,
    feed: null,
    saved: null,
    liked: null,
    tipped: null,
    post: null,
    silhouette: null,
    stripeKey: null,
  },

  actions: {
    getUser ({ commit }) {
      axios.get('/user').then(r => {
        if (r.data != 'False') {
          commit('SET_AUTH', true)
          commit('SET_USER', r.data)
        }else{
          commit('SET_AUTH', false)
          commit('SET_USER', r.data)
        }
      }).catch(error => {
        console.log(error) 
        commit('SET_AUTH', false)
        commit('SET_USER', null)
      })
    },
    getUserProfile({ commit }){
      var f = new FormData()
      f.append('username', this.state.user.username )
      axios.post('/getProfile', f, {headers: {
        'Content-Type': 'multipart/form-data',
          'X-CSRFToken': Cookies.get('csrftoken')}
        }).then( (r) => {
          if(r.data != 'False'){
            commit('SET_PROFILE', r.data)
          }else{
            this.$router.push({name:'createProfile'})
          }
      }).catch( (error) => {
          console.log(error) 
      })
    },
    getCurrUserEvents ({ commit }) {
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getCurrUserEvents', f, {headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': Cookies.get('csrftoken')}
      }).then(r => {
        commit('SET_EVENTS', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getEvents ({ commit }) {
      axios.get('/getPublicEvents').then(r => {
        commit('SET_EVENTS', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getPublicProfiles ({ commit }) {
      axios.get('/getPublicProfiles').then(r => {
        commit('SET_PROFILES', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getProfile({ commit }, payload){
      var f = new FormData()
      f.append('username', payload )
      axios.post('/getProfile', f, {headers: {
        'Content-Type': 'multipart/form-data',
          'X-CSRFToken': Cookies.get('csrftoken')}
        }).then( (r) => {
          commit('SET_SILHOUETTE', r.data)
      }).catch( (error) => {
          this.$emit('error', 'Looks like something went wrong')
          this.loading = false
      })
    },
    getFeed ({ commit }) {
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getFeed', f, {headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': Cookies.get('csrftoken')}
      }).then(r => {
        commit('SET_FEED', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getPost ({ commit }, payload) {
      var f = new FormData()
      f.append('uuid', payload)
      axios.post('/getPost', f, {headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': Cookies.get('csrftoken')}
      }).then(r => {
        commit('SET_POST', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getSavedPosts({ commit }){
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getSavedPosts',f, {headers: {
        'Content-Type': 'multipart/form-data',
          'X-CSRFToken': Cookies.get('csrftoken')}
        }).then( r => {
          commit('SET_SAVED', r.data)
      }).catch(error => {
          console.log(error)
      })
    },
    getLikedPosts({ commit }){
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getLikedPosts',f, {headers: {
          'Content-Type': 'multipart/form-data',
          'X-CSRFToken': Cookies.get('csrftoken')}
        }).then( r => {
          commit('SET_LIKED', r.data)
      }).catch(error => {
          console.log(error)
      })
    },
    getTippedPosts({ commit }){
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getTippedPosts',f, {headers: {
          'Content-Type': 'multipart/form-data',
          'X-CSRFToken': Cookies.get('csrftoken')}
        }).then( r => {
          commit('SET_TIPPED', r.data)
      }).catch(error => {
          console.log(error)
      })
    },
    getFollowsEvents ({ commit }) {
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getFollowsEvents', f, {headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': Cookies.get('csrftoken')}
      }).then(r => {
        commit('SET_EVENTS', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getFriendsEvents ({ commit }) {
      var f = new FormData()
      f.append('username', this.state.user.username)
      axios.post('/getFriendsEvents', f, {headers: {
        'Content-Type': 'multipart/form-data',
        'X-CSRFToken': Cookies.get('csrftoken')}
      }).then(r => {
        commit('SET_EVENTS', r.data)
      }).catch(error => {
        console.log(error) 
      })
    },
    getUsers ({ commit }) {
      axios.get('api/registration/').then(r => {
        commit('SET_USERS', r.data)
      }).catch(error => {
        console.log(error) 
      })
    }
  },

  mutations: {
    SET_AUTH (state, authenticated) {
      state.authenticated = authenticated
    },
    SET_USER (state, user) {
      state.user = user
    },
    SET_EVENTS (state, events) {
      state.events = events
    },
    UPDATE_TOTAL (state, total) {
      state.total = total
    },
    UPDATE_BAG (state, bag) {
      state.bag = bag
    },
    SET_SESSION (state, session) {
      state.session = session
    },
    SET_XP (state, XP) {
      state.XP = XP
    },
    SET_USERS (state, users) {
      state.users = users
    },
    SET_PROFILES(state, profiles){
      state.profiles = profiles
    },
    SET_PROFILE(state, profile){
      state.profile = profile
    },
    SET_FEED(state, feed){
      state.feed = feed
    },
    SET_SAVED(state, saved){
      state.saved = saved
    },
    SET_LIKED(state, liked){
      state.liked = liked
    },
    SET_POST(state, post){
      state.post = post
    },
    SET_SILHOUETTE(state, silhouette){
      state.silhouette = silhouette
    },
    SET_STRIPE_KEY(state, stripeKey){
      state.stripeKey = stripeKey
    },
    SET_TIPPED(state, tipped){
      state.tipped = tipped
    }
  }
})