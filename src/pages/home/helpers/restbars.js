module.exports = [	
	{
		"placeName" : "COLONY Bar",
		"address" : "965 Granville St, Vancouver, BC V6Z 1L3",
		"location" : [
			{
				"lat" : "49.279660",
				"lng" : "-123.122510"
			}
		]
	},
	
	{
		"placeName" : "Roxy Burger",
		"address" : "910 Granville St, Vancouver, BC V6Z 1L2",
		"location" : [
			{
				"lat" : "49.279870",
				"lng" : "-123.121490"
			}
		]
	},
	
	{
		"placeName" : "The GPO",
		"address" : "136 W Cordova St, Vancouver, BC V6B 2N3",
		"location" : [
			{
				"lat" : "49.283200",
				"lng" : "-123.108580"
			}
		]
	},
	
	{
		"placeName" : "Uva",
		"address" : "900 Seymour St, Vancouver, BC V6B 3L9",
		"location" : [
			{
				"lat" : "49.279240",
				"lng" : "-123.120520"
			}
		]
	},
	
	{
		"placeName" : "Malone's",
		"address" : "608 W Pender St, Vancouver, BC V6B 1V8",
		"location" : [
			{
				"lat" : "49.284490",
				"lng" : "-123.114690"
			}
		]
	},
	
	{
		"placeName" : "Red Card",
		"address" : "560 Smithe St, Vancouver, BC V6B 3L9",
		"location" : [
			{
				"lat" : "49.279380",
				"lng" : "123.120070"
			}
		]
	},
	
	{
		"placeName" : "The Cambie Bar & Gril",
		"address" : "300 Cambie St, Vancouver, BC V6B 2N3",
		"location" : [
			{
				"lat" : "49.283210",
				"lng" : "-123.109060"
			}
		]
	},
	
	{
		"placeName" : "Moose's Down Under",
		"address" : "830 W Pender St, Vancouver, BC V6C 1J8",
		"location" : [
			{
				"lat" : "49.285450",
				"lng" : "-123.116650"
			}
		]
	},
	
	{
		"placeName" : "Rogue",
		"address" : "601 W Cordova St, Vancouver, BC V6B 1G1",
		"location" : [
			{
				"lat" : "49.285622",
				"lng" : "-123.111862"
			}
		]
	},
	
	{
		"placeName" : "Original Joe's",
		"address" : "298 Robson St, Vancouver, BC V6B 6A1",
		"location" : [
			{
				"lat" : "49.278780",
				"lng" : "-123.115930"
			}
		]
	},
	
	{
		"placeName" : "Cabana Lounge",
		"address" : "1159 Granville St, Vancouver, BC V6Z 1L5",
		"location" : [
			{
				"lat" : "49.279259",
				"lng" : "-123.123016"
			}
		]
	},
	
	{
		"placeName" : "Granville Room",
		"address" : "957 Granville St, Vancouver, BC V6Z 1L3",
		"location" : [
			{
				"lat" : "49.279869",
				"lng" : "-123.122688"
			}
		]
	},
	
	{
		"placeName" : "Granville After Party Lounge",
		"address" : "1133 Granville St, Vancouver, BC V6Z 1M1",
		"location" : [
			{
				"lat" : "49.277890",
				"lng" : "-123.125160"
			}
		]
	},
	
	{
		"placeName" : "Brix & Mortar",
		"address" : "1138 Homer St, Vancouver, BC V6B 2X6 &, 1137 Hamilton St, Vancouver, BC V6B 5P6",
		"location" : [
			{
				"lat" : "49.275690",
				"lng" : "-123.122190"
			}
		]
	},
	
	{
		"placeName" : "Bridges Restaurant",
		"address" : "1696 Duranleau St, Vancouver, BC V6H 3S4",
		"location" : [
			{
				"lat" : "49.272810",
				"lng" : "-123.136430"
			}
		]
	},
	
	{
		"placeName" : "Showcase",
		"address" : "1122 W Hastings St, Vancouver, BC V6E 4J6",
		"location" : [
			{
				"lat" : "49.288100",
				"lng" : "-123.120510"
			}
		]
	},
	
	{
		"placeName" : "Warehouse",
		"address" : "989 Granville St, Vancouver, BC V6Z 2A8",
		"location" : [
			{
				"lat" : "49.279400",
				"lng" : "-123.122910"
			}
		]
	},
	
	{
		"placeName" : "The Keefer Bar",
		"address" : "135 Keefer St, Vancouver, BC V6A 1X3",
		"location" : [
			{
				"lat" : "49.279550",
				"lng" : "-123.100590"
			}
		]
	},
	
	{
		"placeName" : "Ginger Juice Bar",
		"address" : "1128 W Georgia St, Vancouver, BC V6E 3H7",
		"location" : [
			{
				"lat" : "49.285770",
				"lng" : "-123.123100"
			}
		]
	},
	
	{
		"placeName" : "Rio Brazilian Steakhouse",
		"address" : "1122 Denman St, Vancouver, BC V6G 1T2",
		"location" : [
			{
				"lat" : "49.287941",
				"lng" : "-123.140480"
			}
		]
	},
	
	{
		"placeName" : "Kingyo",
		"address" : "871 Denman St, Vancouver, BC V6G 2L9",
		"location" : [
			{
				"lat" : "49.290610",
				"lng" : "-123.137140"
			}
		]
	},
	
	{
		"placeName" : "Gon's Izakaya",
		"address" : "854 Denman St, Vancouver, BC V6G 2L8",
		"location" : [
			{
				"lat" : "49.290820",
				"lng" : "-123.136120"
			}
		]
	},
	
	{
		"placeName" : "Hook Seabar",
		"address" : "1210 Denman St, Vancouver, BC V6G 2N2",
		"location" : [
			{
				"lat" : "49.287270",
				"lng" : "-123.141690"
			}
		]
	},
	
	{
		"placeName" : "Dockside Restaurant",
		"address" : "1253 Johnston St, Vancouver, BC V6H 3R9",
		"location" : [
			{
				"lat" : "49.269470",
				"lng" : "-123.131271"
			}
		]
	},
	
	{
		"placeName" : "Local Kitsilano",
		"address" : "2210 Cornwall Ave, Vancouver, BC V6K 1B5",
		"location" : [
			{
				"lat" : "49.272350",
				"lng" : "-123.155330"
			}
		]
	},
	
	{
		"placeName" : "The Wolf & Hound",
		"address" : "3617 W Broadway, Vancouver, BC V6R 2B8",
		"location" : [
			{
				"lat" : "49.264380",
				"lng" : "-123.184130"
			}
		]
	},
	
	{
		"placeName" : "Barra 41",
		"address" : "2407 W 41st Ave, Vancouver, BC V6M 2A5",
		"location" : [
			{
				"lat" : "49.234740",
				"lng" : "-123.161430"
			}
		]
	},
	
	{
		"placeName" : "Cravings",
		"address" : "8808 Osler St, Vancouver, BC V6P 4G2",
		"location" : [
			{
				"lat" : "49.205570",
				"lng" : "-123.131660"
			}
		]
	},
	
	{
		"placeName" : "The Buck",
		"address" : "12111 Third Ave, Richmond, BC V7E 3K1",
		"location" : [
			{
				"lat" : "49.125760",
				"lng" : "-123.186200"
			}
		]
	},
	
	{
		"placeName" : "Harold's",
		"address" : "7551 Westminster Hwy, Richmond, BC V6X 1A3",
		"location" : [
			{
				"lat" : "49.170780",
				"lng" : "-123.142021"
			}
		]
	},
	
	{
		"placeName" : "Stanley's",
		"address" : "14140 Triangle Rd, Richmond, BC V6W 1B2",
		"location" : [
			{
				"lat" : "49.136980",
				"lng" : "-123.065500"
			}
		]
	},
	
	{
		"placeName" : "Dominion Bar",
		"address" : "13475 Central Ave Unit D1, Surrey, BC V3T 0L8",
		"location" : [
			{
				"lat" : "49.190720",
				"lng" : "-122.850290"
			}
		]
	},
	
	{
		"placeName" : "Donegal's Irish",
		"address" : "12054 96 Ave, Surrey, BC V3V 1W3",
		"location" : [
			{
				"lat" : "49.176990",
				"lng" : "-122.887590"
			}
		]
	},
	
	{
		"placeName" : "Wild Wing",
		"address" : "7322 King George Blvd #115, Surrey, BC V3W 5A5",
		"location" : [
			{
				"lat" : "49.135270",
				"lng" : "-122.845140"
			}
		]
	},

	{
		"placeName" : "One20",
		"address" : "8037 120 St, Delta, BC V4C 6P7",
		"location" : [
			{
				"lat" : "49.149490",
				"lng" : "-122.890810"
			}
		]
	}
]