import Vue from 'vue'
import App from '@/App.vue'
import * as VueGoogleMaps from 'vue2-google-maps'
import store from './store'
import axios from 'axios'
import router from '@/router'
import VueMoment from 'vue-moment'
// src/plugins/vuetify.js
import vuetify from '@/plugins/vuetify' // path to vuetify export
import moment from 'moment-timezone'
import VueMapbox from "vue-mapbox"
import Mapbox from "mapbox-gl"
import lodash from 'lodash'
import FileUploader from '@/components/FileUploader'
import VueSocialSharing from 'vue-social-sharing'
import VueMasonry from 'vue-masonry-css'
import VueLazyload from 'vue-lazyload'


Vue.use(VueLazyload)

// or with options
const loadimage = require('./assets/load.gif')
const errorimage = require('./assets/error.gif')


Vue.use(VueLazyload, {
  lazyComponent: true,
  preLoad: 1.3,
  error: errorimage,
  loading: loadimage,
  attempt: 1
})

Vue.use(VueMasonry)

Vue.use(VueSocialSharing)

Vue.use(VueMapbox, { mapboxgl: Mapbox })

Object.defineProperty(Vue.prototype, '_', { value: lodash });

Vue.config.productionTip = false

Vue.use(VueMoment, {
    moment,
})

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyA44aoCuEnj2w8iu2rQZneQijpbORTuSrw',
    libraries: 'places', // This is required if you use the Autocomplete plugin
    // OR: libraries: 'places,drawing'
    // OR: libraries: 'places,drawing,visualization'
    // (as you require)
 
    //// If you want to set the version, you can do so:
    // v: '3.26',
  },
 
  //// If you intend to programmatically custom event listener code
  //// (e.g. `this.$refs.gmap.$on('zoom_changed', someFunc)`)
  //// instead of going through Vue templates (e.g. `<GmapMap @zoom_changed="someFunc">`)
  //// you might need to turn this on.
  // autobindAllEvents: false,
 
  //// If you want to manually install components, e.g.
  //// import {GmapMarker} from 'vue2-google-maps/src/components/marker'
  //// Vue.component('GmapMarker', GmapMarker)
  //// then set installComponents to 'false'.
  //// If you want to automatically install all the components this property must be set to 'true':
  installComponents: true
})

Vue.component('fileUploader', FileUploader)

const vue = new Vue({
  router,
  store,
  vuetify,
  render: h => h(App),

}) 

vue.$mount('#app')
