// src/plugins/vuetify.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {theme: {
    themes: {
      light: {
        primary: '#1F2020',
        secondary: '#E4E4E2',
        accent: '#AEEA00',
        error: '#b71c1c',
      },
    },
  },}

export default new Vuetify(opts)