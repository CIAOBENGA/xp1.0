"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
"""
  
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from django.views.generic import RedirectView
from django.views.generic import TemplateView
from django.conf import settings                      # add this 
from django.conf.urls.static import static            # add this    

from .api.views import GetTippedPosts, TipPostSuccess, TipPostCheckout, ReplyComment, GetPostComments, CommentPost, GetLikedPosts, LikePost, UnlikePost, UnsavePost, GetSavedPosts, GetFeed, GetUserPosts, SavePost, GetPost, CreatePost, SendWelcomeEmail, EditUserEmail, SendPromoterLink, CheckStripeKey, GetPublicProfiles, GetCurrUserEvents, GetFriendsEvents, GetFollowsEvents, GetPublicEvents, UnfollowUser, FollowUser, GetFollows, GetFollowers, GetFriends, EditEvent, DeleteEvent, SaveXP, GetStripeDashLink, VendorOrder, GetUserEvents, GetUserOrders, EditProfile, AttachOrderUser, ViewOrder, CreateProfile, GetProfile, SetStripeKey, GetStripeKey, Success, ProfileViewSet, OrderViewSet, Checkout, ViewEvent,Logout, Login, GetUser, CreateEvent, index_view, MessageViewSet, install_view, manifest_view, offline_view, FileViewSet, UserViewSet, EventViewSet, ReverseGeocode
 
router = routers.DefaultRouter()
router.register('messages', MessageViewSet)
router.register('registration', UserViewSet, basename='registration')
router.register('files', FileViewSet)
router.register('event', EventViewSet)
router.register('order', OrderViewSet)
router.register('profile', ProfileViewSet)
 
urlpatterns = [ 

    # http://localhost:8000/
    path('', index_view, name='index'),

    path('search', index_view),

    path('authorize', index_view),

    path('user', GetUser, name='user'),

    path('login', Login, name='login'),

    path('logout', Logout, name='logout'),

    path('events', CreateEvent, name='events'),

    path('editEvent', EditEvent, name='editEvent'),

    path('deleteEvent', DeleteEvent, name='deleteEvent'),

    path('getProfile', GetProfile, name='getProfile'),

    path('createProfile', CreateProfile, name='createProfile'),

    path('editProfile', EditProfile, name='editProfile'),

    path('getStripeKey', GetStripeKey, name='getStripeKey'),

    path('setStripeKey', SetStripeKey, name='setStripeKey'),

    path('viewevent', ViewEvent, name='viewevent'), 

    path('checkout', Checkout, name='checkout'), 

    path('success', Success, name='success'),

    path('saveXP', SaveXP, name='saveXP'),
 
    path('vieworder', ViewOrder, name='vieworder'),

    path('attachOrderUser', AttachOrderUser, name='attachOrderUser'),

    path('getUserEvents', GetUserEvents, name='getUserEvents'),

    path('getUserOrders', GetUserOrders, name='getUserOrders'),

    path('vendor', VendorOrder, name='vendor'),

    path('getStripeDashLink', GetStripeDashLink, name='getStripeDashLink'),

    path('sendPromoterLink', SendPromoterLink, name='sendPromoterLink'),

    path('sendWelcomeEmail', SendWelcomeEmail, name='sendWelcomeEmail'),

    path('checkStripeKey', CheckStripeKey, name='checkStripeKey'),

    path('followUser', FollowUser, name='followUser'),

    path('unfollowUser', UnfollowUser, name='unfollowUser'),

    path('getFollowers', GetFollowers, name='getFollowers'),

    path('getFollows', GetFollows, name='getFollows'),

    path('getFriends', GetFriends, name='getFriends'),

    path('getFriendsEvents', GetFriendsEvents, name='getFriendsEvents'),

    path('getFollowsEvents', GetFollowsEvents, name='getFollowsEvents'),

    path('getPublicEvents', GetPublicEvents, name='getPublicEvents'),

    path('getPublicProfiles', GetPublicProfiles, name='getPublicProfiles'),

    path('getCurrUserEvents', GetCurrUserEvents, name='getCurrUserEvents'),
 
    path('editUserEmail', EditUserEmail, name='editUserEmail'),

    path('reversegeocode', ReverseGeocode, name='reversegeocode'),

    path('createPost', CreatePost, name='createPost'),

    path('getPost', GetPost, name='getPost'),

    path('savePost', SavePost, name='savePost'),

    path('unsavePost', UnsavePost, name='UnsavePost'),

    path('likePost', LikePost, name='likePost'),

    path('unlikePost', UnlikePost, name='UnlikePost'),

    path('getUserPosts', GetUserPosts, name='getUserPosts'),

    path('getFeed', GetFeed, name='getFeed'),

    path('getSavedPosts', GetSavedPosts, name='getSavedPosts'),

    path('getLikedPosts', GetLikedPosts, name='getLikedPosts'),

    path('getTippedPosts', GetTippedPosts, name='getTippedPosts'),

    path('commentPost', CommentPost, name='CommentPost'),

    path('replyComment', ReplyComment, name='replyComment'),

    path('getPostComments', GetPostComments, name='getPostComments'),

    path('tipPostCheckout', TipPostCheckout, name='tipPostCheckout'),

    path('tipPostSuccess', TipPostSuccess, name='tipPostSuccess'),

    path('install', install_view, name='install'),

    path('manifest.json', manifest_view, name='manifest'),
 
    path('offline.html', offline_view, name='offline'),
 
    path('precache-manifest.d1905a89663959c87bb50b91604a9b34.js', (TemplateView.as_view(
        template_name="precache-manifest.d1905a89663959c87bb50b91604a9b34.js",
        content_type="application/javascript",
    )), name="precache-manifest.d1905a89663959c87bb50b91604a9b34.js"),
  
    # http://localhost:8000/api/<router-viewsets>
    path('api/', include(router.urls)),

    # http://localhost:8000/api/admin/
    path('api/admin/', admin.site.urls),

    path('service-worker.js', (TemplateView.as_view(
        template_name="service-worker.js",
        content_type='application/javascript',
    )), name='service-worker.js'),
]
# + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


if settings.DEBUG:     
    urlpatterns += static(settings.STATIC_URL,document_root=settings.STATIC_ROOT)   
    # urlpatterns += static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
    urlpatterns += static('/', document_root='dist')