from django.db import models
from rest_framework import serializers
from jsonfield import JSONField
from .storage_backends import PublicMediaStorage
from django.conf import settings
from django.contrib.auth.models import User 

class Message(models.Model):
    subject = models.CharField(max_length=200)
    body = models.TextField()


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Message
        fields = ('url', 'subject', 'body', 'pk')

class Registration(models.Model):
	username = models.CharField(max_length=200, unique=True)
	email = models.EmailField(max_length=254, unique=True)
	created_at = models.DateTimeField(auto_now_add=True)
		
class RegistrationSerializer(serializers.ModelSerializer):
	"""docstring for RegistrationSerializer"""
	class Meta:
		model = Registration
		fields = ('username', 'email')


class UserSerializer(serializers.ModelSerializer):
    # ....
    class Meta:
        model = User
        fields = ('username', 'email','password')
        write_only_fields = ('password',)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user


class Profile(models.Model):
	"""docstring for UserProfile"""
	profile_id = models.AutoField(primary_key=True)
	user = models.OneToOneField(User,on_delete=models.CASCADE, related_name='profile')
	bio=models.TextField(max_length=1024, default='Am I an android or a quasar?')	
	DOB = models.TextField(max_length=1024, default='')
	location=JSONField(max_length=10240, default='')
	avatar=JSONField(max_length=200, default='')
	privacy=models.CharField(max_length=200, default='everyone')
	XP=models.CharField(max_length=200, default='1000')
	level=models.CharField(max_length=200, default='4')
	stripe_key=models.CharField(max_length=200, default='none')
	saved = models.ManyToManyField('Post', related_name='save_for')
	liked = models.ManyToManyField('Post', related_name='like_for')
	tipped = models.ManyToManyField('Post', related_name='tip_for') 
	tagged = models.ManyToManyField('Post', related_name='tagged_by')
	follows = models.ManyToManyField('Profile', related_name='followed_by')

	def __unicode__(self):
	    return self.user.username



class ProfileSerializer(serializers.ModelSerializer):
	class Meta:
		"""docstring for Meta"""
		model = Profile
		fields = '__all__'
			

class Post(models.Model):
	"""docstring for Post"""
	post_id = models.AutoField(primary_key=True)
	uuid=models.CharField(max_length=200, default='')
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	media=JSONField(max_length=10240, default=list)
	location=JSONField(max_length=10240,default=list)
	caption=models.TextField(max_length=1024, default='')
	group = models.CharField(max_length=200, default='photo')
	repliable = models.CharField(max_length=200, default='false')
	comments = models.ManyToManyField('Comment')
	tipable = models.CharField(max_length=200, default='false')
	# if false posts will still be likable albeit count won't be shown 
	likable =  models.CharField(max_length=200, default='false')
	likes = models.ManyToManyField('Profile', related_name='liked_by')
	tips = models.ManyToManyField('Profile', related_name='tipped_by')
	minTip = models.CharField(max_length=200, default='100')
	TTI = models.CharField(max_length=200, default='false')
	reports = models.ManyToManyField('Profile', related_name='reported_by')
	created_at = models.DateTimeField(auto_now_add=True)


class Comment(models.Model):
	"""docstring for Comment"""
	commment_id = models.AutoField(primary_key=True)
	uuid=models.CharField(max_length=200, default='')
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	content = models.TextField(max_length=1024, default='')
	replies = models.ManyToManyField('Comment')	
	created_at = models.DateTimeField(auto_now_add=True)
		



class Event(models.Model):
	"""docstring for Event"""
	event_id = models.AutoField(primary_key=True)
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	uuid=models.CharField(max_length=200, default='')
	name=models.CharField(max_length=200)
	privacy=models.CharField(max_length=200)
	description=models.TextField(max_length=1024)
	startDate=models.CharField(max_length=200)
	startTime=models.CharField(max_length=200)
	endDate=models.CharField(max_length=200)
	endTime=models.CharField(max_length=200)
	location=JSONField(max_length=10240)
	tags=JSONField(max_length=1024, default=list)
	products=JSONField(max_length=1024)
	images=JSONField(max_length=1024)
	promoters=JSONField(max_length=1024, default=list)
	created_at = models.DateTimeField(auto_now_add=True)

		
class EventSerializer(serializers.ModelSerializer):
	"""docstring for EventSerializer"""
	class Meta:
		model = Event
		fields = ('uuid', 'name', 'description', 'startDate', 'startTime', 'endDate', 'endTime', 'tags', 'location', 'products', 'images', 'privacy', 'promoters')


class File(models.Model):
    file_id = models.AutoField(primary_key=True)
    filename = models.CharField(max_length=200)
    file = models.FileField(storage=PublicMediaStorage()) 
    created_at = models.DateTimeField(auto_now_add=True)


class FileSerializer(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ('filename', 'file', )


class Order(models.Model):
	order_id = models.AutoField(primary_key=True)
	session_id=models.CharField(max_length=200)
	user = models.ForeignKey(User,on_delete=models.CASCADE, default='', blank=True, null=True)
	event = models.ForeignKey(Event, on_delete=models.CASCADE, default='', blank=True, null=True)
	profile = models.ForeignKey(Profile, on_delete=models.CASCADE, default='', blank=True, null=True)
	items=JSONField(max_length=10240, default='', blank=True, null=True)
	currency=models.CharField(max_length=200, default='USD')
	amount=models.CharField(max_length=200)
	session=JSONField(max_length=10240)
	paid=models.CharField(max_length=200, default='false')
	vendored=models.CharField(max_length=200, default='false')
	created_at = models.DateTimeField(auto_now_add=True)

class OrderSerializer(serializers.ModelSerializer):
	class Meta:
		"""docstring for Meta"""
		model = Order
		fields = '__all__'
			
