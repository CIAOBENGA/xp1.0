# Generated by Django 3.0.8 on 2020-07-04 10:08

import backend.api.storage_backends
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import jsonfield.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('event_id', models.AutoField(primary_key=True, serialize=False)),
                ('uuid', models.CharField(default='', max_length=200)),
                ('name', models.CharField(max_length=200)),
                ('privacy', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=1024)),
                ('startDate', models.CharField(max_length=200)),
                ('startTime', models.CharField(max_length=200)),
                ('endDate', models.CharField(max_length=200)),
                ('endTime', models.CharField(max_length=200)),
                ('location', jsonfield.fields.JSONField(max_length=10240)),
                ('tags', jsonfield.fields.JSONField(default=list, max_length=1024)),
                ('products', jsonfield.fields.JSONField(max_length=1024)),
                ('images', jsonfield.fields.JSONField(max_length=1024)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='File',
            fields=[
                ('file_id', models.AutoField(primary_key=True, serialize=False)),
                ('filename', models.CharField(max_length=200)),
                ('file', models.FileField(storage=backend.api.storage_backends.PublicMediaStorage(), upload_to='')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=200)),
                ('body', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Registration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=200, unique=True)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('profile_id', models.AutoField(primary_key=True, serialize=False)),
                ('bio', models.TextField(default='Am I an android or a quasar?', max_length=1024)),
                ('avatar', models.CharField(default='https://s3-us-west-1.amazonaws.com/xp.ciaobenga.com/media/public/sci-fi.png', max_length=200)),
                ('XP', models.CharField(default='1000', max_length=200)),
                ('level', models.CharField(default='4', max_length=200)),
                ('stripe_key', models.CharField(default='none', max_length=200)),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='profile', to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('order_id', models.AutoField(primary_key=True, serialize=False)),
                ('session_id', models.CharField(max_length=200)),
                ('items', jsonfield.fields.JSONField(max_length=10240)),
                ('amount', models.CharField(max_length=200)),
                ('session', jsonfield.fields.JSONField(max_length=10240)),
                ('paid', models.CharField(default='false', max_length=200)),
                ('vendored', models.CharField(default='false', max_length=200)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('event', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Event')),
                ('user', models.ForeignKey(blank=True, default='', null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
