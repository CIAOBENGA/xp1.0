# Generated by Django 3.0.8 on 2020-08-27 08:30

from django.db import migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0004_profile_follows'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='promoters',
            field=jsonfield.fields.JSONField(default=list, max_length=1024),
        ),
    ]
