# Generated by Django 3.0.8 on 2020-09-29 22:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_auto_20200928_2220'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='gavatar',
            field=models.CharField(default='1', max_length=200),
        ),
    ]
