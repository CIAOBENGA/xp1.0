from django.views.generic import TemplateView
from django.http import HttpResponse, JsonResponse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt
from django.forms.models import model_to_dict
from django.shortcuts import redirect
from rest_framework import viewsets, generics, mixins
from django.template.loader import render_to_string
from django.views import View
from rest_framework.views import APIView 
from .models import Comment, Post, Profile, ProfileSerializer, Order, OrderSerializer, Message, MessageSerializer, RegistrationSerializer, Registration, File, FileSerializer, Event, EventSerializer, UserSerializer
from django.contrib.auth.models import User 
from django.contrib.auth import authenticate, login, logout
from .forms import UserForm
import googlemaps
from datetime import datetime  
import json
import stripe
import os
from sendgrid import SendGridAPIClient
from sendgrid.helpers.mail import Mail

# SF
# Serve Application
index_view = never_cache(TemplateView.as_view(template_name='index.html'))
#Serve Install Page
install_view = never_cache(TemplateView.as_view(template_name='install.html'))
manifest_view = never_cache(TemplateView.as_view(template_name='manifest.json'))
offline_view = never_cache(TemplateView.as_view(template_name='offline.html'))

class MessageViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
 

class UserViewSet(viewsets.ModelViewSet):   
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @csrf_exempt
    def post(request):
    	serializer = UserSerializer(request.username, request.email, request.password)
    	if serializer.is_valid():
    		serializer.save()
    		login(request, serializer)
    		return Response(request.user, status=status.HTTP_201_CREATED)
    	return Response(False, status=status.HTTP_400_BAD_REQUEST)

class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer

class ProfileViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows messages to be viewed or edited.
    """
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer

@csrf_exempt
def EditUserEmail(request):
    u = User.objects.get(username=request.POST.get('username'))
    u.email = request.POST.get('email')
    u.save()
    return HttpResponse(True)

@csrf_exempt
def SendWelcomeEmail(request):
    rendered = render_to_string('welcome.html', {'username': request.POST.get('username')})
    message = Mail(
        from_email='house@ciaobenga.com',
        to_emails=request.POST.get('email'),
        subject='EXPERIENCE',
        html_content=rendered)
    sg = SendGridAPIClient('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')
    response = sg.send(message)
    return HttpResponse('Success')

@csrf_exempt
def GetProfile(request):    
    u = User.objects.get(username=request.POST.get('username'))
    if Profile.objects.filter(user=u).exists():
        profile = Profile.objects.get(user=u)
        profile_dict = model_to_dict(profile)
        profile_dict.pop('follows')
        profile_dict.pop('saved')
        profile_dict.pop('liked')
        profile_dict.pop('tagged')
        profile_dict.pop('tipped')
        if u == request.user:
            return HttpResponse(json.dumps({'profile': profile_dict, 'owner': True}))
        else:
            return HttpResponse(json.dumps({'profile': profile_dict, 'owner': False}))
    else:
        return HttpResponse(False)
    
@csrf_exempt
def CreateProfile(request):
    profile = Profile(user=request.user, bio=request.POST.get('bio'), avatar=request.POST.get('avatar'), location=request.POST.get('location'), privacy=request.POST.get('privacy'), DOB=request.POST.get('DOB'))
    profile.save()
    profile_dict = model_to_dict(profile)
    return HttpResponse(json.dumps(profile_dict))

@csrf_exempt
def CreatePost(request):
    post = Post(user=request.user, uuid=request.POST.get('uuid'), media=request.POST.get('media'), location=request.POST.get('geocode'), group=request.POST.get('group'), caption=request.POST.get('caption'), repliable=request.POST.get('repliable'), likable=request.POST.get('likable'), tipable=request.POST.get('tipable'), TTI=request.POST.get('TTI'), minTip=request.POST.get('minTip'))
    post.save()
    post_dict = model_to_dict(post)
    return HttpResponse(json.dumps(post_dict))

# Get post
@csrf_exempt 
def GetPost(request):
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    if post:
        user = User.objects.get(username=post.user) 
        user_dict = model_to_dict(user)
        post_dict = model_to_dict(post)

        post_dict.pop('likes')
        post_dict.pop('comments')
        post_dict.pop('tips')
        post_dict.pop('reports')

        user_dict.pop('date_joined')
        user_dict.pop('last_login')

        profile = Profile.objects.get(user=post.user)
        profile_dict = model_to_dict(profile)
        profile_dict.pop('follows')
        profile_dict.pop('saved')
        profile_dict.pop('liked')
        profile_dict.pop('tagged')
        profile_dict.pop('tipped')
        if post.user == request.user:
            return HttpResponse(json.dumps({'post': post_dict, 'user': user_dict, 'profile': profile_dict, 'owner': True}))
        else:
            return HttpResponse(json.dumps({'post': post_dict, 'user': user_dict, 'profile': profile_dict, 'owner': False}))
    else:
        return HttpResponse(False)
    
@csrf_exempt
def SavePost(request):
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    profile = Profile.objects.get(user=request.user)
    profile.saved.add(post) 
    post.save()
    return HttpResponse(True)

@csrf_exempt
def UnsavePost(request):
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    profile = Profile.objects.get(user=request.user)
    profile.saved.remove(post) 
    post.save()
    return HttpResponse(True)

# get following posts
def GetFeed(request):
    _u = User.objects.get(username=request.user)
    profile = Profile.objects.get(user=_u)
    follows = profile.follows.all()
    post_list = []

    for follow in follows:
        u = User.objects.get(username=follow.user)
        profile = Profile.objects.get(user=follow.user)

        user_dict = model_to_dict(follow.user)
        user_dict.pop('date_joined')
        user_dict.pop('last_login')
        user_dict.pop('password')

        profile_dict = model_to_dict(profile)
        profile_dict.pop('follows')
        profile_dict.pop('saved')
        profile_dict.pop('liked')
        profile_dict.pop('tagged')
        profile_dict.pop('tipped')

        if Post.objects.filter(user=u).exists():
            posts = Post.objects.filter(user=u).order_by('-created_at')          
            for post in posts:
                _dict = model_to_dict(post)
                _dict.pop('tips')
                _dict.pop('likes')
                _dict.pop('comments')
                _dict.pop('reports')
                
                post_list.append({'post': _dict, 'user': user_dict, 'profile': profile_dict})

    return HttpResponse(json.dumps(post_list))


def GetUserPosts(request):
    u = User.objects.get(username=request.POST.get('username'))
    post_list = [] 
    if Post.objects.filter(user=u).exists():
        posts = Post.objects.filter(user=u).order_by('-created_at')           
        for post in posts:
            _dict = model_to_dict(post)
            _dict.pop('tips')
            _dict.pop('likes')
            _dict.pop('comments')
            _dict.pop('reports')
            post_list.append(_dict)

        return HttpResponse(json.dumps(post_list))
    else:
        return HttpResponse(True)
        
# get saved posts
def GetSavedPosts(request):
    profile = Profile.objects.get(user=request.user)
    posts = profile.saved.all()
    post_list = []

    for post in posts:
        _dict = model_to_dict(post)
        _dict.pop('tips')
        _dict.pop('likes')
        _dict.pop('comments')
        _dict.pop('reports')
        post_list.append(_dict)
 
    return HttpResponse(json.dumps(post_list))

# get saved posts
def GetTippedPosts(request):
    profile = Profile.objects.get(user=request.user)
    posts = profile.tipped.all()
    post_list = []

    for post in posts:
        _dict = model_to_dict(post)
        _dict.pop('tips')
        _dict.pop('likes')
        _dict.pop('comments')
        _dict.pop('reports')
        post_list.append(_dict)
 
    return HttpResponse(json.dumps(post_list))

# comment post    
def CommentPost(request):
    # retrieve post
    post = Post.objects.get(uuid=request.POST.get('puid'))
    # create comment
    comment = Comment(user=request.user, uuid=request.POST.get('uuid'), content=request.POST.get('comment'))
    comment.save()

    # add comment to post
    post.comments.add(comment)

    return HttpResponse(True)

# reply to comment
def ReplyComment(request):
    # retrieve comment
    comment = Comment.objects.get(uuid=request.POST.get('cuid'))
     # create comment 
    reply = Comment(user=request.user, uuid=request.POST.get('uuid'), content=request.POST.get('comment'))
    reply.save()
    # add comment to comment
    comment.replies.add(reply)

    return HttpResponse(True)

# get post comments (with replies)
def GetPostComments(request):
    # retrieve post
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    # retrieve post comments
    comments = post.comments.all()
    comment_list = []
    reply_list = []

    for comment in comments:
        _dict = model_to_dict(comment)
        user_dict = model_to_dict(comment.user)
        username = user_dict.pop('username')

        profile = Profile.objects.get(user=comment.user)
        profile_dict = model_to_dict(profile)
        avatar = profile_dict.pop('avatar')

        replies = _dict.pop('replies')


        for reply in replies:
            reply_dict = model_to_dict(reply)
            reply_list.append(reply_dict)

        comment_list.append({'username': username, 'avatar': avatar, 'comment': _dict, 'replies': reply_list})

    return HttpResponse(json.dumps(comment_list))
    # return json

# # get comment replies
# def GetCommentReplies(request):
#     # retrieve comment
#     comment = Comment.objects.get(uuid=request.POST.get('uuid'))
#     # retrieve post replies
#     replies = comment.replies.all()
#     reply_list = []

#     for reply in replies:
#         _dict = model_to_dict(reply)
#         _dict.pop('replies')
#         reply_list.append(_dict)

#     return HttpResponse(json.dumps(reply_list))

# tip post checkout
def TipPostCheckout(request):
    profile = Profile.objects.get(user=request.POST.get('user'))
    merchant_key = request.POST.get('merchant_key')
    # sk_test_8qoe76ZCWiFtyIKqghwKNIw200ORj3EwLh
    #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
    stripe.api_key = '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'

    session = stripe.checkout.Session.create(
      payment_method_types=['card'],
      line_items=[{
        'name': request.POST.get('name'),
        'description': request.POST.get('description'),
        'amount': request.POST.get('amount'),
        'currency': request.POST.get('currency'),
        'quantity': 1,
      }],
      mode='payment',
      success_url='https://expxp.com/#/tip/success/{CHECKOUT_SESSION_ID}',
      cancel_url='https://expxp.com/#/p/'+request.POST.get('uuid'),
    ) 

    stripe.PaymentIntent.modify(
      session.payment_intent,
      metadata={'amount': request.POST.get('amount'), 'merchant_key': merchant_key, 'payout': request.POST.get('payout'), 'puid': request.POST.get('uuid')}
    )

    order =  Order(session_id=session.id, user=request.user, profile=profile, event=None, amount=request.POST.get('amount'), session=session)
    order.save()

    return HttpResponse(json.dumps(session))

# Tip Post Success
def TipPostSuccess(request):
    obj = Order.objects.get(session_id=request.POST.get('session'))
    # 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
    # sk_test_8qoe76ZCWiFtyIKqghwKNIw200ORj3EwLh
    stripe.api_key = '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' 
    session = stripe.checkout.Session.retrieve(request.POST.get('session'))
    intent = stripe.PaymentIntent.retrieve(session.payment_intent)
    if obj.paid == 'false':
        stripe.Transfer.create(
          amount=intent.metadata.payout,
          currency="usd",
          source_transaction=intent.charges.data[0].id,
          destination=intent.metadata.merchant_key,
        )
        obj.paid = 'true'
        obj.save()

        profile = Profile.objects.get(user=request.user)
        post = Post.objects.get(uuid=intent.metadata.puid)
        profile.tipped.add(post)
        post.tips.add(profile)


    obj_dict = model_to_dict(obj)
    return HttpResponse(json.dumps({'order': obj_dict, 'post': intent.metadata.puid}))

# get post tips
# tag post
# get tagged posts
# report posts

# like post (double tap)
def LikePost(request):
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    profile = Profile.objects.get(user=request.user)
    profile.liked.add(post) 
    post.likes.add(profile)

    return HttpResponse(True)

# unlike post (triple tap)
def UnlikePost(request):
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    profile = Profile.objects.get(user=request.user)
    profile.liked.remove(post) 
    post.likes.remove(profile)

    return HttpResponse(True)

# get saved posts
def GetLikedPosts(request):
    profile = Profile.objects.get(user=request.user)
    posts = profile.liked.all()
    post_list = []

    for post in posts:
        _dict = model_to_dict(post)
        _dict.pop('tips')
        _dict.pop('likes')
        _dict.pop('comments')
        _dict.pop('reports')
        post_list.append(_dict)

    return HttpResponse(json.dumps(post_list))

# get post likes
def GetPostLikes(request):
    post = Post.objects.get(uuid=request.POST.get('uuid'))
    likes = post.likes.all()

    likes_list = []

    for like in likes:
        _dict = model_to_dict(like)
        likes_list.append(_dict)

    return HttpResponse(json.dumps(likes_list))

@csrf_exempt
def EditProfile(request): 
    if Profile.objects.filter(user=request.user).exists():
        profile = Profile.objects.get(user=request.user)
        profile.bio = request.POST.get('bio')
        profile.avatar = request.POST.get('avatar')
        profile.privacy = request.POST.get('privacy')
        profile.location = request.POST.get('location')
        profile.DOB = request.POST.get('DOB')

        profile.save()

        return HttpResponse(True)
    else:
        return HttpResponse(False)

@csrf_exempt
def FollowUser(request):
    u = User.objects.get(username=request.POST.get('username'))
    user_profile = Profile.objects.get(user=request.user)
    profile = Profile.objects.get(user=u)
    user_profile.follows.add(profile) 
    user_profile.save()

    return HttpResponse(True)

    #Friends are followers you following
    #Add option to create events, just for friends, for all followers, for everyone
    #tim.userprofile.follows.all() # list of userprofiles of users that tim follows
    #chris.userprofile.followed_by.all() # list of userprofiles of users that follow chris

@csrf_exempt
def UnfollowUser(request):
    u = User.objects.get(username=request.POST.get('username'))
    user_profile = Profile.objects.get(user=request.user)
    profile = Profile.objects.get(user=u)
    user_profile.follows.remove(profile) 
    user_profile.save()

    return HttpResponse(True)

@csrf_exempt
def GetFollowers(request):
    u = User.objects.get(username=request.POST.get('username'))
    profile = Profile.objects.get(user=u)
    followers = profile.followed_by.all()
    followers_list = []

    for follower in followers:
        _dict = model_to_dict(follower)
        _dict.pop('follows')
        _dict.pop('saved')
        _dict.pop('liked')
        _dict.pop('tagged')
        _dict.pop('tipped')
        user_follower = User.objects.get(username=follower.user)
        user_dict = model_to_dict(user_follower)
        user_dict.pop('date_joined')
        user_dict.pop('last_login')
        followers_list.append({'profile': _dict, 'user': user_dict })

    return HttpResponse(json.dumps(followers_list))


@csrf_exempt
def GetFollows(request):
    u = User.objects.get(username=request.POST.get('username'))
    profile = Profile.objects.get(user=u)
    follows = profile.follows.all()
    follows_list = []

    for follow in follows:
        _dict = model_to_dict(follow)
        _dict.pop('follows')
        _dict.pop('saved')
        _dict.pop('liked')
        _dict.pop('tagged')
        _dict.pop('tipped')
        user_follow = User.objects.get(username=follow.user)
        user_dict = model_to_dict(user_follow)
        user_dict.pop('date_joined')
        user_dict.pop('last_login')
        follows_list.append({'profile': _dict, 'user': user_dict })

    return HttpResponse(json.dumps(follows_list))


@csrf_exempt
def GetFriends(request):
    u = User.objects.get(username=request.POST.get('username'))
    profile = Profile.objects.get(user=u)
    follows = profile.follows.all()
    followers = profile.followed_by.all()
    followers_list = []
    follows_list=[]
    friend_list = []

    for follow in follows:
        follow_dict = model_to_dict(follow)
        follows_list.append(follow_dict)

    for follower in followers:
        follower_dict = model_to_dict(follower)
        followers_list.append(follower_dict)


    for x in follows_list:
        for y in followers_list:
            if x['profile_id'] == y['profile_id']:
                y.pop('follows')
                y.pop('saved')
                y.pop('liked')
                y.pop('tagged')
                y.pop('tipped')
                user_friend = User.objects.get(pk=y['user'])
                user_dict = model_to_dict(user_friend)
                user_dict.pop('date_joined')
                user_dict.pop('last_login')
                friend_list.append({'profile': y, 'user': user_dict })

    return HttpResponse(json.dumps(friend_list))           

@csrf_exempt
def GetFriendsEvents(request):
    u = User.objects.get(username=request.POST.get('username'))
    profile = Profile.objects.get(user=u)
    follows = profile.follows.all()
    followers = profile.followed_by.all()
    followers_list = []
    follows_list=[]
    event_list = []

    for follow in follows:
        follow_dict = model_to_dict(follow)
        follows_list.append(follow_dict)

    for follower in followers:
        follower_dict = model_to_dict(follower)
        followers_list.append(follower_dict)


    for x in follows_list:
        for y in followers_list:
            if x['profile_id'] == y['profile_id']:
                y.pop('follows')
                u_ = User.objects.get(pk=y['user'])
                if Event.objects.filter(user=u_).exists():
                    events = Event.objects.filter(user=u_)          
                    for event in events:
                        _dict = model_to_dict(event)
                        event_list.append({'event':_dict, 'profile': y})

    return HttpResponse(json.dumps(event_list))

@csrf_exempt
def GetFollowsEvents(request):
    u = User.objects.get(username=request.POST.get('username'))
    profile = Profile.objects.get(user=u)
    follows = profile.follows.all()
    follows_list = []
    event_list = []

    for follow in follows:
        u_ = User.objects.get(username=follow.user)
        profile = Profile.objects.get(user=u_)
        profile_dict = model_to_dict(profile)
        profile_dict.pop('follows')
        if Event.objects.filter(user=u_).exists():
            events = Event.objects.filter(username=follow.user)          
            for event in events:
                _dict = model_to_dict(event)
                event_list.append({'event':_dict, 'profile': profile_dict})

    return HttpResponse(json.dumps(event_list))

@csrf_exempt
def GetPublicEvents(request):
    events = Event.objects.filter(privacy='everyone')     
    event_list = []     
    for event in events:
        _dict = model_to_dict(event)
        u_ = User.objects.get(username=event.user)
        profile = Profile.objects.get(user=u_)
        profile_dict = model_to_dict(profile)
        profile_dict.pop('follows')
        event_list.append({'event':_dict, 'profile': profile_dict})

    return HttpResponse(json.dumps(event_list))

@csrf_exempt
def GetPublicProfiles(request):
    profiles = Profile.objects.filter(privacy='everyone')
    profile_list = []
    for profile in profiles: 
        _dict = model_to_dict(profile)
        u = User.objects.get(username=profile.user)
        username = u.username
        _dict.pop('follows')
        _dict.pop('saved')
        _dict.pop('liked')
        _dict.pop('tagged')
        _dict.pop('tipped')
        profile_list.append({'profile': _dict, 'username': username})

    profile_list.reverse()
    return HttpResponse(json.dumps(profile_list))

@csrf_exempt
def GetUserEvents(request):
    u = User.objects.get(username=request.POST.get('username'))
    if Event.objects.filter(user=u).exists():
        events = Event.objects.filter(user=u)
        event_list = []
        for event in events:
            _dict = model_to_dict(event)
            event_list.append(_dict)
        return HttpResponse(json.dumps(event_list))
    else:
        return HttpResponse(False)

@csrf_exempt
def GetCurrUserEvents(request):
    u = User.objects.get(username=request.POST.get('username'))
    profile = Profile.objects.get(user=u)
    profile_dict = model_to_dict(profile)
    profile_dict.pop('follows')
    if Event.objects.filter(user=u).exists():
        events = Event.objects.filter(user=u)
        event_list = []
        for event in events:
            _dict = model_to_dict(event)
            event_list.append({'event':_dict, 'profile': profile_dict})
        return HttpResponse(json.dumps(event_list))
    else:
        return HttpResponse(False)

@csrf_exempt
def GetUserOrders(request):
    u = User.objects.get(username=request.POST.get('username'))
    if Order.objects.filter(user=u).exists():
        orders = Order.objects.filter(user=u)
        order_list = []
        for order in orders:
            _dict = model_to_dict(order)
            order_list.append(_dict)
        return HttpResponse(json.dumps(order_list))
    else:
        return HttpResponse(False)

@csrf_exempt
def GetEventOrders(request):
    event = Event.objects.get(uuid=request.POST.get('uuid'))
    if event:
        event_dict = model_to_dict(event)
        orders = Order.objects.filter(event=event)
        if orders:
            order_list = []
            for order in orders:
                _dict = model_to_dict(order)
                order_list.append(_dict)
            return HttpResponse(json.dumps({'event': event_dict, 'orders': order_list}))
        else:
            return HttpResponse(json.dumps({'event': event_dict, 'orders': 'None'}))
    else:
        return HttpResponse('No event associated with that uuid')

@csrf_exempt
def GetUser(request):
    user = request.user
    if request.user.is_authenticated:
        serializer = UserSerializer(user)                                                                                                                                                                                                                                                                                                                                                    
        return HttpResponse(json.dumps(serializer.data))
    else:
        # Return an 'invalid login' error message.
        return HttpResponse(False)

@csrf_exempt 
def ReverseGeocode(request):
    gmaps = googlemaps.Client(key='AIzaSyDUWZb8it1krDGElPNJejf3k5uJUkPLyhs')
    reverse_geocode_result = gmaps.reverse_geocode((request.POST.get('lat'), request.POST.get('lng')))
    return HttpResponse(json.dumps({'location': reverse_geocode_result}))


@csrf_exempt
def Login(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return HttpResponse(request.user)
    else:
        # Return an 'invalid login' error message.
        return HttpResponse(False)

@csrf_exempt
def Logout(request):
    logout(request)
    return redirect('/')

@csrf_exempt
def GetStripeKey(request):
    stripe_key = request.user.profile.stripe_key
    return HttpResponse(stripe_key)

@csrf_exempt
def CheckStripeKey(request):
    u = User.objects.get(username=request.POST.get('username'))
    if Profile.objects.filter(user=u).exists():
        profile = Profile.objects.get(user=u)
        if profile.stripe_key != 'none':
            return HttpResponse(True)
        else:
            return HttpResponse(False)
    else: 
        return HttpResponse(False)

@csrf_exempt
def SetStripeKey(request):
    profile = Profile.objects.get(user=request.user)
    # sk_test_8qoe76ZCWiFtyIKqghwKNIw200ORj3EwLh
    #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
    stripe.api_key = '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'

    response = stripe.OAuth.token(
      grant_type='authorization_code',
      code=request.POST.get('stripeKey'),
    )

    # Access the connected account id in the response
    connected_account_id = response['stripe_user_id']
    profile.stripe_key = connected_account_id
    profile.save()

    return HttpResponse(True)

@csrf_exempt
def SaveXP(request):
    profile = Profile.objects.get(user=request.user)
    profile.XP = request.POST.get('XP')
    profile.level = request.POST.get('level')
    profile.save()
    profile_dict = model_to_dict(profile)

    return HttpResponse(json.dumps(profile_dict))
    
@csrf_exempt
def GetStripeDashLink(request):
    if request.user.is_authenticated:

       profile = Profile.objects.get(user=request.user)
       # sk_test_8qoe76ZCWiFtyIKqghwKNIw200ORj3EwLh
       # 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
       stripe.api_key = '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'
       dash_link = stripe.Account.create_login_link(profile.stripe_key)
       return HttpResponse(json.dumps(dash_link))
    else:
        return HttpResponse('User must be logged in to view your dashboard')

@csrf_exempt
def CreateEvent(request):
    user = request.user
    if user is not None:
        event = Event(user = user, uuid = request.POST.get('uuid'), name = request.POST.get('name'), privacy = request.POST.get('privacy'), description = request.POST.get('description'), startDate = request.POST.get('startDate'), startTime = request.POST.get('startTime'), endDate = request.POST.get('endDate'), endTime = request.POST.get('endTime'), location = request.POST.get('location'), tags = request.POST.get('tags'), products = request.POST.get('products'), images = request.POST.get('images'), promoters = request.POST.get('promoters'))
        event.save()
        event_dict = model_to_dict(event)
        return HttpResponse(json.dumps(event_dict))
    else:
        return HttpResponse('User must be logged in to create an event')


@csrf_exempt
def ViewEvent(request):
    obj = Event.objects.get(uuid=request.POST.get('uuid'))
    if obj:
        obj_dict = model_to_dict(obj)
        return HttpResponse(json.dumps(obj_dict))
    else:
        return HttpResponse('No event associated with that uuid')

@csrf_exempt
def EditEvent(request):
    event = Event.objects.get(uuid=request.POST.get('uuid'))
    if event:
        event.name = request.POST.get('name') 
        event.privacy = request.POST.get('privacy') 
        event.description = request.POST.get('description')
        event.startDate = request.POST.get('startDate')
        event.startTime = request.POST.get('startTime')
        event.endDate = request.POST.get('endDate')
        event.endTime = request.POST.get('endTime') 
        event.location = request.POST.get('location') 
        event.tags = request.POST.get('tags')
        event.products = request.POST.get('products')
        event.promoters = request.POST.get('promoters')
        event.images = request.POST.get('images') 
        event.save()
        event_dict = model_to_dict(event)
        return HttpResponse(json.dumps(event_dict))
    else:
        return HttpResponse('No event associated with that uuid')

@csrf_exempt
def DeleteEvent(request):
    obj = Event.objects.get(uuid=request.POST.get('uuid'))
    if obj:
        obj.delete()
        return HttpResponse(True)
    else:
        return HttpResponse('No event associated with that uuid')

@csrf_exempt
def SendPromoterLink(request):
    rendered = render_to_string('promoter.html', {'username': request.POST.get('username'), 'link': request.POST.get('link'), 'commission': request.POST.get('commission')})
    message = Mail(
        from_email='house@ciaobenga.com',
        to_emails=request.POST.get('email'),
        subject='Hooray! You have been selected to promote on XP',
        html_content=rendered)
    sg = SendGridAPIClient('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')
    response = sg.send(message)
    return HttpResponse('Success')

@csrf_exempt
def Checkout(request):
    if request.POST.get('route_name') != 'viewUser':
        event = Event.objects.get(pk=request.POST.get('event_id'))
        merchant_key = Profile.objects.get(user=event.user).stripe_key
    else:
        profile = Profile.objects.get(user=request.POST.get('user'))
        merchant_key = request.POST.get('merchant_key')

    # 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
    stripe.api_key = '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'  
    session = stripe.checkout.Session.create(
      payment_method_types=['card'],
      line_items=[{
        'name': request.POST.get('name'),
        'description': request.POST.get('description'),
        'amount': request.POST.get('amount'),
        'currency': request.POST.get('currency'),
        'quantity': 1,
      }],
      mode='payment',
      success_url='https://expxp.com/#/success/{CHECKOUT_SESSION_ID}',
      cancel_url='https://expxp.com/#/e/cancel',
    )

    stripe.PaymentIntent.modify(
      session.payment_intent,
      metadata={'merchant_key': merchant_key, 'payout': request.POST.get('payout')}
    )

    if request.POST.get('route_name') == 'pViewEvents':
        u = User.objects.get(username=request.POST.get('pid'))
        profile = Profile.objects.get(user=u)
        paymentIntent = stripe.PaymentIntent.modify(
          session.payment_intent,
          metadata={'promoter_key': profile.stripe_key, 'commission': request.POST.get('commission_amount')}
        )

    user = request.user

    # if user is not None:
    #     orderUser = user 
    # else:
    #     orderUser = None
    if request.POST.get('route_name') != 'viewUser':
        order =  Order(session_id=session.id, user=None, profile=None, event=event, items=request.POST.get('items'), amount=request.POST.get('amount'), session=session)
        order.save()
    else:   
        order =  Order(session_id=session.id, user=None, profile=profile, event=None, items=request.POST.get('items'), amount=request.POST.get('amount'), session=session)
        order.save()
        
    return HttpResponse(json.dumps(session))


@csrf_exempt
def Success(request): 
    # Fix error
    obj = Order.objects.get(session_id=request.POST.get('session'))
    if obj:
        if obj.event_id is None:
            profile = Profile.objects.get(pk=obj.profile_id)
            loaded_inv = json.loads(profile.products)
        else:   
            event = Event.objects.get(pk=obj.event_id)
            loaded_inv = json.loads(event.products)
       
        loaded_obj = json.loads(obj.items)
        

        if obj.paid == 'false':
            # Transfer funds to promoters
            # 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
            stripe.api_key = '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'  
            session = stripe.checkout.Session.retrieve(request.POST.get('session'))
            intent = stripe.PaymentIntent.retrieve(session.payment_intent)
            stripe.Transfer.create(
              amount=intent.metadata.payout,
              currency="usd",
              source_transaction=intent.charges.data[0].id,
              destination=intent.metadata.merchant_key,
            )
            if 'promoter_key' in intent.metadata:
                stripe.Transfer.create(
                  amount=intent.metadata.commission,
                  currency="usd",
                  source_transaction=intent.charges.data[0].id,
                  destination=intent.metadata.promoter_key,
                )

            for x in loaded_obj:
                for y in loaded_inv:
                    if x['name'] == y['name']:
                        quantity = int(y['quantity']) 
                        quantity-=1
                        y['quantity'] = str(quantity)

            if obj.event_id is None:
                profile.products = json.dumps(loaded_inv, separators=(',', ':'))
                profile.save()
            else:
                event.products = json.dumps(loaded_inv, separators=(',', ':'))
                event.save()
                
            obj.paid = 'true'
            obj.save()

        obj_dict = model_to_dict(obj)
        return HttpResponse(json.dumps(obj_dict))
    else:
        return HttpResponse('No order associated with that session')

@csrf_exempt    
def ViewOrder(request): 
    obj = Order.objects.get(session_id=request.POST.get('session_id'))
    if obj: 
        event = Event.objects.get(pk=obj.event_id)
        obj_dict = model_to_dict(obj)
        event_dict = model_to_dict(event)
        if event.user == request.user:
            return HttpResponse(json.dumps({'order': obj_dict, 'owner': True, 'event': event_dict}))
        else:
            return HttpResponse(json.dumps({'order': obj_dict, 'owner': False, 'event': event_dict}))
    else:
        return HttpResponse('No order associated with that session')

@csrf_exempt
def AttachOrderUser(request):
    obj = Order.objects.get(session_id=request.POST.get('session'))
    if obj:
        obj.user = request.user
        obj.save()
        obj_dict = model_to_dict(obj)
        return HttpResponse(json.dumps(obj_dict))
    else:
        return HttpResponse('No order associated with that session')

@csrf_exempt
def VendorOrder(request):
    obj = Order.objects.get(session_id=request.POST.get('session_id'))
    if obj:
        obj.vendored = 'true'
        obj.save()
        obj_dict = model_to_dict(obj)
        return HttpResponse(json.dumps(obj_dict))
    else:
        return HttpResponse('No order associated with that session')



class FileViewSet(viewsets.ModelViewSet):
    queryset = File.objects.all()
    serializer_class = FileSerializer

    @csrf_exempt
    def post(request):
        serializer = FileSerializer(request.filename, request.file)
        if serializer.is_valid():
            serializer.save()   
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class EventViewSet(viewsets.ModelViewSet):
    queryset = Event.objects.all()
    serializer_class = EventSerializer

    @csrf_exempt
    def post(request):
    	serializer = EventSerializer(request.uuid, request.name, request.description, request.startDate, request.startTime, request.endDate, request.endTime, request.tags, request.location, request.promoters, request.tickets, request.images, request.privacy)
    	if serializer.is_valid():
    		serializer.save()
    		return Response(serializer.data, status=status.HTTP_201_CREATED)
    	else:
    		return Response('No account associated with email, create account to continue', status=status.HTTP_400_BAD_REQUEST)




